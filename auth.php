<?php

if (!($_SERVER['REQUEST_METHOD'] === 'POST')) {
	echo 'This page expects POST method.';
	http_response_code(405);
	header('Location: /login.html');
	die();
}

require 'open_db.php';

if (isset($_COOKIE['secret'])) {
	$secret = SQLite3::escapeString($_COOKIE['secret']);
	$fields = ['login', 'secret'];

	$result = $db->query('SELECT '.implode(",",$fields).' FROM users WHERE secret="' . $secret . '"');

	if ($result == false) {
		echo 'Secret is invalid';
		setcookie('secret', '', 0, "/");
	} else {
		while($row = $result->fetchArray(SQLITE3_ASSOC)) {
			if ($row['secret'] == $secret)
			{
				echo 'Your login is ' . $row['login'];
				exit;
			}
		}
	}

	exit();
}

$account_found = false;
if (isset($_POST['login']) && isset($_POST['pass'])) {
	
	$login = SQLite3::escapeString($_POST['login']);
	
	$fields = ['login', 'pass', 'email', 'secret'];
	$result = $db->query('SELECT '.implode(",",$fields).' FROM users WHERE login="' . $login . '"');
	
	$row = $result->fetchArray(SQLITE3_ASSOC);
	
	// if no lines were found
	if ($row == false) {
		echo 'Login was not found!';
	} else {
		if ($login == $row['login'] 
			&& hash('sha256', substr($row['email'], 0, 4) . $_POST['pass']) == $row['pass']) {
			echo 'Successful logged in!';
			$account_found = true;

			setcookie('secret', $row['secret'], time() + (86400 * 30), "/");
		} else {
			header('Location: /login.html?error=password-wrong');
			exit();
		}
	}
} else {
	header('Location: /login.html');
	exit();
}

if (!$account_found) {
	header('Location: /login.html?error=accound-absend');
	exit();
}

