﻿<?php

function random_str(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
): string {
    if ($length < 1) {
        throw new \RangeException("Length must be a positive integer");
    }
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}

if (!($_SERVER['REQUEST_METHOD'] === 'POST')) {
	echo 'This page expects POST method.';
	http_response_code(405);
	header('Location: /register.html');
	die();
}

$fields = array(
	'email' => 'email-required',
	'login' => 'login-required',
	'pass' => 'pass-required',
	'pass-repeat' => 'pass-repeat-required',
);

foreach ($fields as $key => $value) {
	if (!isset($_POST[$key])) {
		header('Location: /register.html?error=' . $value);
		exit();
	}
}

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
	header('Location: /register.html?error=bad-email');
	exit();
}
	
if (!filter_var($_POST['login'], FILTER_VALIDATE_REGEXP,
			   array('options' => array('regexp' => '/[a-zA-Z0-9_-]{3,}/')))) {
	header('Location: /register.html?error=bad-login');
	exit();
}
	
if (!filter_var($_POST['pass'], FILTER_VALIDATE_REGEXP,
			   array('options' => array('regexp' => '/[^\s]{4,}/')))) {
	header('Location: /register.html?error=bad-password');
	exit();
}
	
if ($_POST['pass-repeat'] != $_POST['pass']) {
	header('Location: /register.html?error=wrong-repeat');
	exit();
}


require 'open_db.php';

$login = SQLite3::escapeString($_POST['login']);

$email = SQLite3::escapeString($_POST['email']);
$pass = SQLite3::escapeString(hash('sha256', substr($email, 0, 4) . $_POST['pass']));

$secret = random_str(128);

$query = 'insert into users (login, pass, email, secret) values ("'.$login.'", "'.$pass.'", "'.$email.'","' . $secret . '");';

$success = $db->exec($query);

if ($success) {
	echo 'Successfully registered!';
} else {
	echo 'Something gone wrong.';	
}