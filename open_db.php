<?php

$database_name = 'exuser.db';

class MyDB extends SQLite3 {
    function __construct() {
		global $database_name;
       	$this->open($database_name);
    }
 }

$db = new MyDB();

if(!$db) {
    echo $db->lastErrorMsg();
	throw new Exception('Database \'' . $database_name . '\' failed to open!');
}
?>
